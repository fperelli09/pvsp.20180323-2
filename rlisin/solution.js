process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function largestPermutation(k, n, arr) {
    var position = arr.reduce(function(c, num, index) {
        c[num] = index;
        return c;
    }, []);

    var i = 0;
    while(i < n && k > 0) {
        if (n - i !== arr[i]) {
            var current = position[n - i];
            position[n - i] = [i, position[arr[i]] = position[n - i]][0];
            arr[current] = [arr[i], arr[i] = arr[current]][0];
            k--;
        }
        i++;
    }

    return arr;
}

function main() {
    var n_temp = readLine().split(' ');
    var n = parseInt(n_temp[0]);
    var k = parseInt(n_temp[1]);
    arr = readLine().split(' ');
    arr = arr.map(Number);
    var result = largestPermutation(k, n, arr);
    console.log(result.join(" "));



}
