process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
    input_stdin += data;
});

process.stdin.on('end', function () {
    input_stdin_array = input_stdin.split("\n");
    main();
});

function readLine() {
    return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////
// 5 1
// 4 2 3 5 1 -> 5 2 3 4 1
//
// 5 2
// 4 2 3 5 1 -> 5 2 3 4 1 -> 5 4 3 2 1


Array.prototype.swap = function(a, b) {
	let tmp = this[a];
	this[a] = this[b];
	this[b] = tmp;
}

function largestPermutation(k, arr) {
		const n = arr.length;
    let index = {};

    arr.forEach((currentVal, currentIndex) => {
      index[currentVal] = currentIndex;
    });

		for(let i = 0; k > 0 && i < n; i++){
			let value = arr[i], ideal = n - i;
			if (arr[i] < ideal) {
				arr.swap(i, index[ideal]);
				index[value] = index[ideal];
				delete index[ideal];
				k--;
			}
		}

		return arr;
}

function main() {
    var n_temp = readLine().split(' ');
    var n = parseInt(n_temp[0]);
    var k = parseInt(n_temp[1]);
    arr = readLine().split(' ');
    arr = arr.map(Number);
    var result = largestPermutation(k, arr);
    console.log(result.join(" "));



}
